---
title: Getting Started
---

docopt-turtle-template has three commands, `cat`, `echo` and `lstree`. Here are some example use cases

```bash
app-exe cat foo.txt
```

```bash
app-exe echo --caps foo # prints "FOO"
```

```bash
app-exe lstree
```
