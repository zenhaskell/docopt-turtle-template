---
title: Home
---

# docopt-turtle-template - A simple CLI application using docopt and turtle

This is the soft documentation site for the docopt-turtle-template.

This serves as a place to provide detailed descriptions about how to
use your application or library.

![build](https://gitlab.com/zenhaskell/docopt-turtle-template/badges/master/build.svg)
