# docopt-turtle-template

A simple CLI application using [docopt](https://github.com/docopt/docopt.hs)
and [turtle](https://hackage.haskell.org/package/turtle). This builds in
gitlab's ci to produce an output binary.

## Building

Build with

    stack build

Run with

    stack exec -- app-exe

## Prototyping

You may edit the `USAGE.txt` to edit the docopt instructions at compile time.
They will be reflected in the output binary. To make use of them simply add any
a `when` directive in `app/Main.hs` as demonstrated.
